from pprint import pprint
import copy
import itertools
import sys


def full_balanced_update(data, service, count):
    result = copy.deepcopy(data)

    cluster_names = sorted(result.keys())

    for i in range(count):
        cluster_loads = []
        full_load = 0

        for cluster in cluster_names:
            current_cluster_load = sum(result[cluster].values())

            cluster_loads.append((cluster, current_cluster_load))
            full_load += current_cluster_load

        min_cluster = min(cluster_loads, key=lambda x: (x[1]))
        min_cluster_name = min_cluster[0]

        if service not in result[min_cluster_name]:
            result[min_cluster_name][service] = 0

        result[min_cluster_name][service] += 1

    return result


def predictable_round_update(data, service, count):
    result = copy.deepcopy(data)

    cluster_names = sorted(result.keys())

    cluster_loads = []
    full_load = 0

    for cluster in cluster_names:
        current_cluster_load = sum(result[cluster].values())

        cluster_loads.append((cluster, current_cluster_load))
        full_load += current_cluster_load

    min_cluster = min(cluster_loads, key=lambda x: (result[x[0]].get(service, 0), x[0]))
    min_cluster_name = min_cluster[0]

    cycle_names = itertools.cycle(sorted(cluster_names))
    while next(cycle_names) != min_cluster_name:
        pass

    for i in range(count):
        if service not in result[min_cluster_name]:
            result[min_cluster_name][service] = 0

        result[min_cluster_name][service] += 1

        min_cluster_name = next(cycle_names)

    return result


def update(data, service, count):
    return predictable_round_update(data, service, count)


def main():
    example_data = {
        'ginger': {
            'django': 2,
            'flask': 3,
        },
        'cucumber': {
            'flask': 1,
        },
    }

    print("Configuration before:")
    pprint(example_data)

    example_data = update(example_data, 'pylons', 7)

    print("Configuration after:")
    pprint(example_data)


def interactive_mode():
    config = {
        'ginger': {},
        'cucumber': {},
    }

    while True:
        service, count = input().split()
        count = int(count)

        config = update(config, service, count)

        pprint(config)


if __name__ == '__main__':
    if "-i" in sys.argv:
        interactive_mode()
    else:
        main()
